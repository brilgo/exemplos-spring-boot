package com.brilgo.exemplos.spring.boot.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication(scanBasePackages = {"com.brilgo.exemplos.spring"})
public class MainApp {

	private static final Logger LOGGER = LoggerFactory.getLogger(MainApp.class);
	
	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(MainApp.class, args);
		LOGGER.info(ctx.getDisplayName());
		LOGGER.info("This is my first Spring Boot Example");
	}
}

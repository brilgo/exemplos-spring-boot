package com.brilgo.exemplos.spring.boot.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/helps")
public class HelpsController {

	@RequestMapping("/ping")
	public String ping(){
		return "Pong";
	}
}

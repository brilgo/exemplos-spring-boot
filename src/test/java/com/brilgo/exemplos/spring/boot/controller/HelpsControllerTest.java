package com.brilgo.exemplos.spring.boot.controller;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class HelpsControllerTest {
	
	private HelpsController cut;
	
	@Before
	public void init() {
		cut = new HelpsController();
	}
	
	@Test
	public void pingCenarioComValorDeRetornoPongEsperaRecebeOValorPong() {
		// Arrange
		String actual = "Pong";
		
		// Act
		String expected = cut.ping();
		
		// Assert
		assertEquals(expected, actual);
	}
}

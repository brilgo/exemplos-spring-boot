# Sprint Boot Example

This is an example of Spring Boot project using Gradle as package and build manager.

To start the application, open the command line prompt in your machine, enter the path that this project was cloned and enter the following command:

gradle clean build bootRun

Notice: Gradle is a prerequisite to be installed before run this command.